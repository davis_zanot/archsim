nodes = new Array();
edges = [];
components = [];
controls = [];
r = null;
graph = null;
sim = false;
pathfollowing = true;
heatmap = false;
totalPackets = 1;
function printXDOT() {
    $.ajax({
        url: 'dot/xml.xml.txt',
        dataType: "text", // <=== new bit
        success: function(data) {

            parsePlainText(data, 1000);

        }
    });
}
;

function parsePlainText(data, width) {
    var lines = data.split("\n");
    var scale = width / parseFloat(lines[0].split(' ')[2]);
    var scale = scale / parseFloat(lines[0].split(' ')[1]);
    var height = parseFloat(lines[0].split(' ')[3]) * scale;

    r = Raphael("paper", width, height);
    var stopbutton = "M5.5,5.5h20v20h-20z"//stop
    var playbutton = "M6.684,25.682L24.316,15.5L6.684,5.318V25.682z";//play    
    var button = r.path(playbutton).attr({fill: "#0f0", stroke: "#0f0", "stroke-width": 2});//button to change from play to stop
    var pathbutton = r.text(150, 25, "Path Traversal").transform("S2");
    var heatbutton = r.text(300, 25, "Heat Map").transform("S2");

    var visbuttonattrs = [{fill: "#00f", stroke: "#00f", opacity: 0.3}, {fill: "#00f", stroke: "#00fs", opacity: 1}];
    pathbutton.now = 1;
    heatbutton.now = 0;
    pathbutton.attr(visbuttonattrs[pathbutton.now]);
    heatbutton.attr(visbuttonattrs[heatbutton.now]);
    pathbutton.node.onclick = function() {
        pathfollowing = !pathfollowing;
        pathbutton.stop().animate(visbuttonattrs[+(pathbutton.now = !pathbutton.now)], 250);
    };
    heatbutton.node.onclick = function() {
        heatmap = !heatmap;
        heatbutton.stop().animate(visbuttonattrs[+(heatbutton.now = !heatbutton.now)], 250);

    };
    buttonattrs = [{fill: "#f00", stroke: "#f00", path: stopbutton}, {fill: "#0f0", stroke: "#0f0", path: playbutton}];
    button.now = 1;
    button.node.onclick = function() {
        sim = !sim;
        button.stop().animate(buttonattrs[+(button.now = !button.now)], 250);
    };
    graph = r.set();
    (function() {
        Raphael.fn.addGuides = function() {
            this.ca.guide = function(g) {
                return {
                    guide: g
                };
            };
            this.ca.along = function(percent) {
                var box = this.getBBox(false);
                var g = this.attr("guide");
                var len = g.getTotalLength();
                var point = g.getPointAtLength(percent * len);
                var t = {
                    transform: "...T" + [point.x - (box.x + (box.width / 2)), point.y - (box.y + (box.height / 2))]
                };
                return t;
            };
        };
    })();
    r.addGuides();

    for (var i = 0; i < lines.length; i++) {
        if (lines[i].indexOf("node") >= 0) {
            var coordexp = /(\d+\.\d*\s){4}/;
            var coords = coordexp.exec(lines[i])[0].split(" ");
            var x0 = parseFloat(coords[0]);
            var y0 = parseFloat(coords[1]);
            var w0 = parseFloat(coords[2]);
            var h0 = parseFloat(coords[3]);
            var w = w0 * scale;//converting from graphviz output coordinate system to rapheal coordinate system
            var h = h0 * scale;
            var x = (x0) * scale - w / 2;
            var y = height - (y0) * scale - h / 2;
            var label = lines[i].split('\"')[3];
            var node = r.rect(x, y, w, h);
            var fcolor = "#ccccff";
            var scolor = "#070";
            var idexp = /([\s\]\[\w-]*)<([\s\]\[\w-]*)>:([\s\]\[\w-]*)<([\s\]\[\w-]*)>/g;
            var result = idexp.exec(lines[i]);
            node.id = result[1];
            node.container = result[3];
            node.containerName = result[4];
            var transform = "S.4,1";
            node.transform(transform);
            node.attr({fill: fcolor, stroke: scolor, title: node.id, "stroke-width": 4});
            var nodeName = r.text(x + w / 2, y + h / 2, label);
            //node.scale(0.5,0.0);
            nodes.push(node);
            graph.push(node);

        }
        else if (lines[i].indexOf('edge') >= 0) {
            var pathString = "M";
            var pointsexp = /(\d+)\s((\d+.\d+)\s)+/;
            var result = pointsexp.exec(lines[i]);
            var points = result[0].split(" ");
            //console.log(points);
            for (var j = 0; j < points[0]; j++) {
                var x = points[j * 2 + 1] * scale;//converting from graphviz output coordinate system to rapheal coordinate system
                var y = height - points[j * 2 + 2] * scale;
                pathString += x + " " + y + " ";
                if (j === 0) {
                    pathString += "C ";
                }
            }
            var edge = r.path(pathString);

            scolor = '#0f0';
            edge.id = lines[i].split('\"')[3] + ":" + lines[i].split('\"')[7];
            edge.head = lines[i].split('\"')[3];
            edge.tail = lines[i].split('\"')[1];
            edge.numPackets = 0;
            edge.maxPackets = 7;
            edge.attr({stroke: scolor, "stroke-width": 15});
            edge.toBack();
            graph.push(edge);
            edges.push(edge);

        }
    }

    var remainingNodes = nodes.slice(0);//clone array
    while (remainingNodes.length > 0) {
        var nextNode = remainingNodes.shift();//bounding boxes        
        var componentID = nextNode.container;
        var label = nextNode.containerName;
        var xlist = [];
        var ylist = [];
        xlist.push(nextNode.attrs.x);
        xlist.push(nextNode.attrs.x + nextNode.attrs.width);
        ylist.push(nextNode.attrs.y);
        ylist.push(nextNode.attrs.y + nextNode.attrs.height);
        var interfaces = [];
        interfaces.push(nextNode);
        for (var i = 0; i < remainingNodes.length; i++) {
            if (remainingNodes[i].container == componentID) {
                nextNode = remainingNodes.splice(i, 1)[0];
                interfaces.push(nextNode);
                i--;
                xlist.push(nextNode.attrs.x);
                xlist.push(nextNode.attrs.x + nextNode.attrs.width);
                ylist.push(nextNode.attrs.y);
                ylist.push(nextNode.attrs.y + nextNode.attrs.height);
            }
        }

        var x = Math.min.apply(null, xlist);
        var y = Math.min.apply(null, ylist);
        var w = Math.max.apply(null, xlist) - x;
        var h = Math.max.apply(null, ylist) - y;
        var container = r.rect(x, y, w, h);
        container.interfaces = interfaces;
        var scolor = "#000000";
        var transform = "S1.1,.87";
        container.transform(transform);
        container.id = componentID;
        if (componentID.indexOf("connector") > -1) {

            var fcolor = "#ccffcc";
            container.attr({r: 5, fill: fcolor, stroke: scolor, title: container.id, "stroke-width": 4});
        } else {

            var fcolor = "#ffccff";
            container.attr({fill: fcolor, stroke: scolor, title: container.id, "stroke-width": 4});
        }
        var containerName = r.text(x + w / 2, y + h / 2, label);
        var textTransform = "S3,3";
        //containerName.transform(textTransform);
        container.mousedown(function() {
            nodePress(this);
        });
        graph.push(container);
        components.push(container);

    }

    var graphTranslate = "t" + width / 2 + "," + height / 2;
    //setInteval(graph.transform(graphTranslate),1000/60);
    console.log(nodes);

    var init = self.setInterval(function() {
        runSimulation()
    }, 300);

}
;
function runSimulation() {
    console.log(sim);
    if (sim == true) {
        var component = components[Math.floor(Math.random() * components.length)];
        if (component != null)
            if (component.id.indexOf("connector") < 0)
                nodePress(component);
        //setTimeout(runSimulation(), 300+Math.floor(Math.random() * 1700) );

    }
}
function nodePress(node) {
    for (var i = 0; i < node.interfaces.length; i++) {
        if(pathfollowing){
            sendMessage(node.interfaces[i]);
        }
        if(heatmap){
            sendHeat(node.interfaces[i]);
            
        }
    }



}
;
function sendMessage(interface) {
    console.log(interface.id)
    for (var i = 0; i < edges.length; i++) {
        var index = edges[i].id.indexOf(interface.id)
       
        if (pathfollowing) {
            if (edges[i].tail.indexOf(interface.id) > -1) {
                drawPacket(edges[i], "reverse");
            } else if (edges[i].head.indexOf(interface.id) > -1) {
                drawPacket(edges[i], "forward");

            }
        }
    }

}
;
function sendHeat(interface) {
    console.log(interface.id)
    for (var i = 0; i < edges.length; i++) {
        var index = edges[i].id.indexOf(interface.id)
        if (heatmap) {
            if (edges[i].tail.indexOf(interface.id) > -1) {
                drawHeat(edges[i], "reverse");
            } else if (edges[i].head.indexOf(interface.id) > -1) {
                drawHeat(edges[i], "forward");

            }
        }
    }

}
;
function propagatePacket(interface) {
    //console.log(interface);
    var interfaceid = interface.split('<')[0];
    var containerid = interface.split(':')[1].split('<')[0];
    //console.log(interfaceid, containerid);
    var dobreak = false;
    for (var i = 0; i < nodes.length; i++) {
        //console.log(i);
        if (nodes[i].container == containerid && nodes[i].id != interfaceid)
        {
            sendMessage(nodes[i]);
        }
    }
}
;
function propagateHeat(interface) {
    //console.log(interface);
    var interfaceid = interface.split('<')[0];
    var containerid = interface.split(':')[1].split('<')[0];
    //console.log(interfaceid, containerid);
    var dobreak = false;
    for (var i = 0; i < nodes.length; i++) {
        //console.log(i);
        if (nodes[i].container == containerid && nodes[i].id != interfaceid)
        {
            sendHeat(nodes[i]);
        }
    }
}
;
function drawHeat(element, direction) {
    element.numPackets++;
    totalPackets++;
    var modifier = 255;
    var color = "rgb(" + ((element.numPackets / (totalPackets - element.numPackets)) * modifier * 3 ) + "," + (((totalPackets - element.numPackets) / totalPackets) * modifier) + ",0)";

    element.animate({stroke: color}, 1000, function() {
        element.numPackets--;
        totalPackets--;
        if (direction == "forward")
            propagateHeat(element.tail);
        else if (direction == "reverse")
            propagateHeat(element.head);
        color = "rgb(" + ((element.numPackets / (totalPackets - element.numPackets)) * modifier * 3 ) + "," + (((totalPackets - element.numPackets) / totalPackets) * modifier) + ",0)";
        element.animate({stroke: color}, 300);
    });
}
function drawPacket(element, direction) {
    var radius = 6;
    if (direction == "forward") {
        var e = r.ellipse(element.attrs.path[0][1], element.attrs.path[0][2], radius, radius).attr({stroke: "#000", fill: "#f00"});
        e.toFront();
        graph.push(e);
        e.attr({guide: element, along: 1}).animate({along: 0}, 1000, "linear", function() {
            e.attr({rx: 0, ry: 0});
            e.toBack();
            propagatePacket(element.tail);

        });

    } else if (direction == "reverse") {
        var e = r.ellipse(element.attrs.path[0][1], element.attrs.path[0][2], radius, radius).attr({stroke: "#000", fill: "#f00"});
        e.toFront();
        graph.push(e);
        e.attr({guide: element, along: 0}).animate({along: 1}, 1000, "linear", function() {
            e.attr({rx: 0, ry: 0});
            e.toBack();
            propagatePacket(element.head);

        });
        
    }


}
;
function random(min, max) {
    Math.floor(Math.random() * (max - min) * min);
}
