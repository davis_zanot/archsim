<?php

/*
 *component, connector, and link ID's should start with 
 *"component","connector", or "link" as this information
 *is needed when dot generates the txt file of the graph
 *
 *
 */
require_once 'GraphViz.php';


function parseXadl($source) {
    $xml = simplexml_load_string(file_get_contents($source));
    $components;
    $connectors;
    foreach ($xml->children('structure_3_0', true) as $child) {//structure level    
        foreach ($child->children('structure_3_0', true) as $child1) {//links, components, and connector level.        
            if ($child1->getName() == 'component') {
                $components[(string) $child1['id']] = array("name" => (string) $child1['name']);
                foreach ($child1->children('structure_3_0', true)as $child2) {
                    if ($child2->getName() == 'interface') {
                        $components[(string) $child1['id']]['interfaces'][(string) $child2['id']] = array("name" => (string) $child2['name']); 
                    }
                }
            } else if ($child1->getName() == 'connector') {
                $connectors[(string) $child1['id']] = array("name" => (string) $child1['name']);
                foreach ($child1->children('structure_3_0', true)as $child2) {
                    if ($child2->getName() == 'interface') {
                        $connectors[(string) $child1['id']]['interfaces'][(string) $child2['id']] = array("name" => (string) $child2['name']); 
                    }
                }
            } else if ($child1->getName() == 'link') {
                $links[(string) $child1['id']] = array("name" => (string) $child1['name']);
                foreach ($child1->children('structure_3_0', true)as $child2) {//looking for the 2 interfaces of a link
                    if (0 === strpos($child2->getName(), 'point')) {//if the child is 1 of 2 points
                        $links[(string) $child1['id']][$child2->getName()]['interface'] = (string) $child2;
                        
                        foreach ($components as $key => $value) {
                            foreach ($value['interfaces'] as $key1 => $interface) {
                                if ($key1 == (string) $child2) {
                                    $links[(string) $child1['id']][$child2->getName()]['node'] = $key; //stoing the node id associated with the link                                
                                    $links[(string) $child1['id']][$child2->getName()]['nodename'] = $value['name'];
                                    $links[(string) $child1['id']][$child2->getName()]['interfacename'] = $interface['name'];
                                }
                            }
                        }
                        foreach ($connectors as $key => $value) {
                            //var_dump($value);
                            foreach ($value['interfaces'] as $key1 => $interface) {
                                if ($key1 == (string) $child2) {
                                    $links[(string) $child1['id']][$child2->getName()]['node'] = $key; //stoing the node id associated with the link                                
                                    $links[(string) $child1['id']][$child2->getName()]['nodename'] = $value['name'];
                                    $links[(string) $child1['id']][$child2->getName()]['interfacename'] = $interface['name'];
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    //Start Construction Of the graph
    $dot = "graph G {\n";
    foreach ($components as $key => $node) {
        $dot .= "subgraph \"cluster_".$key."\" {\n";//interfaces are grouped by connecor/component in a dot subgraph
        $dot .= "label = \"".$node['name']."\";\n";
        $index = 0;
        foreach ($node['interfaces'] as $key1 => $interface) {           
            $dot .= "\"".$key1."<".$interface['name'].">:".$key."<".$node['name'].">\" [label = \"\"; shape = rect];\n";
        }
        $dot .= "}\n";    
        

    }
    foreach ($connectors as $key => $node) {
        $dot .= "subgraph \"cluster_".$key."\" {\n";//interfaces are grouped by connecor/component in a dot subgraph
        $dot .= "label = \"".$node['name']."\";\n";
        $index = 0;
        foreach ($node['interfaces'] as $key1 => $interface) {
            $dot .= "\"".$key1."<".$interface['name'].">:".$key."<".$node['name'].">\" [label = \"\"; shape = rect];\n";
        }
        $dot .= "}\n"; 
    }
    foreach ($links as $key => $link) {
        $dot .= "\"".$link['point1']['interface']."<".$link['point1']['interfacename'].">:".$link['point1']['node']."<".$link['point1']['nodename'].">\" -- \"".$link['point2']['interface']."<".$link['point2']['interfacename'].">:".$link['point2']['node']."<".$link['point2']['nodename'].">\";\n";
    }    
    $dot .= "}";    
    $input = $source . '.dot';
    file_put_contents($input, $dot);
    $output = $source;
    
     //saves dot file
    $graphviz = new Image_Graphviz(false);
    $graphviz->renderDotFile($input,  $output . '.png', 'png', 'dot'); //saves png file
    $graphviz->renderDotFile($input,  $output . '.xdot', 'xdot', 'dot'); //saves xdot file
    $graphviz->renderDotFile($input,  $output . '.txt', 'plain', 'dot'); //saves plain file
    $graphviz->renderDotFile($input,  $output . '.svg', 'svg', 'dot'); //saves svg file
    
}

?>
